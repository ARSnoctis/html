/**
 * Created by Grzegorz Flakowicz on 14.06.2020.
 */
function runScripts() {
    window.addEventListener("DOMContentLoaded", runAllScripts, false);
}

function runAllScripts() {
    runClock();
    videoAdopt();
}
runScripts();

function videoAdopt() {
    var scalingFactor = 0.72;
    var videoWidth = 1920;
    var videoHeight = 1080;
    var videoFactor = videoWidth/videoHeight;
    var screenWidth = window.innerWidth;
    var screenHeight = window.innerHeight;
    var setWidth;
    var setHeight;
    if (screenHeight*scalingFactor <= screenHeight) {
        setHeight = screenHeight*scalingFactor*videoFactor <= screenWidth*scalingFactor
            ? screenHeight*scalingFactor
            : screenWidth*scalingFactor/videoFactor;
        setWidth = screenWidth*scalingFactor/videoFactor <= screenHeight*scalingFactor
            ? screenWidth*scalingFactor
            : screenHeight*scalingFactor*videoFactor;
    } else {
        setHeight = videoHeight;
        setWidth = videoWidth;
    }
    document.getElementById("video").style.width = setWidth + "px";
    document.getElementById("video").style.height = setHeight + "px";

}

window.onresize = videoAdopt

