/**
 * Created by Grzegorz Flakowicz on 14.06.2020.
 */
function updateClock() {
    var currentTime = new Date();
    var weekday = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

    var currentSeconds = currentTime.getSeconds();
    var currentMinutes = currentTime.getMinutes();
    var currentHours = currentTime.getHours();
    var currentDayOfWeek = weekday[currentTime.getDay()];
    var currentDay = currentTime.getDate();
    var currentMonth = currentTime.getMonth()+1;
    var currentYear = currentTime.getFullYear();

    // Add 0 to maintain double digit format
    currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
    currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
    currentHours = ( currentHours < 10 ? '0' : "") + currentHours;
    currentDay = ( currentDay < 10 ? '0' : "") + currentDay;
    currentMonth = ( currentMonth < 10 ? '0' : "") + currentMonth;


    // Compose the string for display
    var currentTimeString = currentYear + "." + currentMonth + "." + currentDay + " " + currentDayOfWeek + " "+currentHours+":"+currentMinutes+":"+currentSeconds;

    // Update the time
    document.getElementById("clock").innerHTML = currentTimeString;
}

function runClock() {
    updateClock()
    setInterval('updateClock()', 1000);
}