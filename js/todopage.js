/**
 * Created by Grzegorz Flakowicz on 14.06.2020.
 */

var tasks = [];

function runScripts() {
    window.addEventListener("DOMContentLoaded", runAllScripts, false);
}

function runAllScripts() {
    runClock();
    if (sessionStorage.tasks) {
      tasks = JSON.parse(sessionStorage.tasks);
      for (var i=0; i<tasks.length; i++){
          loadTask(tasks[i]);
      }
  }
}
runScripts();




function makeVisible(elementID) {
  document.getElementById(elementID).style.display = "block";
}

function hideElement(elementID) {
  document.getElementById(elementID).style.display = "none";
}

function TaskEntity(title, desc, state) {
  this.title = title;
  this.description = desc;
  this.state = state; 
}

function addTask() {
  var title = document.getElementById("task-title").value.toString();
  var description = document.getElementById("task-description").value.toString();
  if (title === "") {
    document.getElementById('task-form').classList.add('was-validated');
    return;
  };
  if (title != "") {
    var check = document.getElementById(title);
    if (typeof(check) != 'undefined' && check != null)
    {
      document.getElementById("task-form").reset();
      document.getElementById('task-form').classList.add('was-validated');
      return;
    }
  }
  var task = new TaskEntity(title, description, "processing");
  tasks.push(task);
  sessionStorage.tasks = JSON.stringify(tasks);

  loadTask(task);
  document.getElementById("task-form").reset();
  document.getElementById('task-form').classList.remove('was-validated');
}

function loadTask(task) {
  var taskList = document.getElementById("task-list");
  var newTask = createTask(task.title, task.description, task.state);
  taskList.appendChild(newTask);
}

function createTask(title, description, state) {
  var task = document.createElement("li");
  task.setAttribute("class", "list-group-item d-flex justify-content-between lh-condensed");
  task.setAttribute("id", title);
  if (state === "done") {
    task.classList.add("task-in-progress");
  }
  if (state === "ready") {
    task.classList.add("task-done");
  }
  var content = createContentSet(title, description);
  var buttons = createButtonSet(state, title);

  task.appendChild(content);
  task.appendChild(buttons);
  return task;
}

function createButtonSet(process, title) {
  var buttonSet = document.createElement("div");
  buttonSet.setAttribute("class", "btn-group");
  buttonSet.setAttribute("role", "group");
  
  var readyBtn = document.createElement("button");
  readyBtn.setAttribute("id", title + "-" + process);
  readyBtn.setAttribute("type", "button");
  readyBtn.setAttribute("class", "btn btn-snd");
  readyBtn.setAttribute("onclick", "changeState('" + title + "','" + process + "');");
  if (process === "done") {
    readyBtn.appendChild(document.createTextNode("Done"));
  }
  if (process === "ready") {
    readyBtn.appendChild(document.createTextNode("Ready"));
  }
  if (process === "processing") {
    readyBtn.appendChild(document.createTextNode("In progress"));
  }
  var closeBtn = document.createElement("button");
  closeBtn.setAttribute("type", "button");
  closeBtn.setAttribute("class", "btn btn-snd");
  closeBtn.setAttribute("onclick", "removeTask('" + title + "');");
  closeBtn.appendChild(document.createTextNode("Close"));

  buttonSet.appendChild(readyBtn);
  buttonSet.appendChild(closeBtn);
  return buttonSet;
}

function createContentSet(titleEntry, descriptionEntry) {
  var content = document.createElement("div");
  
  var title = document.createElement("h6");
  title.setAttribute("class", "my-0");
  title.appendChild(document.createTextNode(titleEntry));
  
  var desc = document.createElement("small");
  desc.setAttribute("class", "text-muted");
  desc.appendChild(document.createTextNode(descriptionEntry));
  
  content.appendChild(title);
  content.appendChild(desc);
  return content;
}

function removeTask(id) {
  var task = document.getElementById(id);
  console.log(id);
  task.remove();
  for (var i = 0; i < tasks.length; i++) {
    if (tasks[i].title === id) {
      tasks.splice(i, 1);
      sessionStorage.tasks = JSON.stringify(tasks);
    }
  }
}

function changeState(title, state) {
  var task = document.getElementById(title);
  var processBtn = document.getElementById(title + "-" + state);
  if (state === "processing") {
    var process = "done";
    processBtn.setAttribute("id", title + "-" + process);
    processBtn.setAttribute("onclick", "changeState('" + title + "','" + process + "');");
    processBtn.innerHTML = "Done";
    task.classList.add("task-in-progress");
    updateTask(title, process);
  }
  if (state === "done") {
    var process = "ready";
    processBtn.setAttribute("id", title + "-" + process);
    processBtn.setAttribute("onclick", "changeState('" + title + "','" + process + "');");
    processBtn.innerHTML = "Ready";
    task.classList.remove("task-in-progress");
    task.classList.add("task-done");
    updateTask(title, process);
  }
  if (state === "ready") {
    var process = "processing";
    processBtn.setAttribute("id", title + "-" + process);
    processBtn.setAttribute("onclick", "changeState('" + title + "','" + process + "');");
    processBtn.innerHTML = "In progress";
    task.classList.remove("task-done");
    updateTask(title, process);
  }
}

function updateTask(title, process) {
  for (var i = 0; i < tasks.length; i++) {
    if (tasks[i] != undefined && tasks[i].title === title) {
      tasks[i].state = process;
      sessionStorage.tasks = JSON.stringify(tasks);
    }
  }
}
